# Generated by Django 3.1 on 2020-08-06 07:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0002_auto_20200806_1707'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productinorder',
            name='price_item',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='productinorder',
            name='total_price',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
    ]
