from rest_framework import serializers

from storehouse.orders.models import Order, Customer, ProductInOrder


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = '__all__'


class ProductInOrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductInOrder
        fields = ['product', 'quantity', 'price_item', 'total_price']


class OrderDetailSerializer(serializers.ModelSerializer):
    products = ProductInOrderSerializer()

    class Meta:
        model = Order
        fields = '__all__'
        #depth = 1
        # fields = ['number', 'providers']


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = '__all__'

