from django.urls import path

from storehouse.orders.views import OrderView, CustomerView, OrderDetailView

urlpatterns = [
    path('orders/', OrderView.as_view()),
    path('orders/<int:pk>/', OrderDetailView.as_view()),
    path('customers/', CustomerView.as_view()),
]