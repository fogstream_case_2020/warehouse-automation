from django.db import models


class Category(models.Model):
    """
    Категории
    """
    name = models.CharField(max_length=100,
                            verbose_name='Название')
    parent = models.ForeignKey('self',
                               blank=True,
                               null=True,
                               verbose_name='Родительская категория',
                               related_name='children',
                               on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name = 'Категория товара'
        verbose_name_plural = 'Категории товаров'


class Product(models.Model):
    """
    Товары
    """
    name = models.CharField(max_length=100,
                            verbose_name='Наименование')
    vendor_code = models.IntegerField(verbose_name='Артикул')
    price_item = models.DecimalField(max_digits=10,
                                     decimal_places=2,
                                     verbose_name='Цена за ед.')
    quantity_all = models.PositiveIntegerField(verbose_name='Всего')
    quantity_available = models.PositiveIntegerField(verbose_name='Доступно')
    quantity_reserved = models.PositiveIntegerField(verbose_name='Зарезервировано')
    category = models.ForeignKey(Category,
                                 blank=True,
                                 null=True,
                                 default=None,
                                 on_delete=models.SET_NULL,
                                 verbose_name='Категория')

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'
